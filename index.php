<?php

//Fonctionnalités présentes :
//  Enregistrement d'un client en BDD
//  connexion via nom, mail et mdp
//  catalogue
//  panier dispo quand connecté
//  validation du panier puis affichage du résumé de commande
//
//PAs encore mis en place :
//   partie admin
//   Sécurité et vérifications des champs de saisie



//Autoloading before the session_start to be able to store object in the session
require 'app/Autoloader.php';
Autoloader::register();
session_start();

//Debugging function
function dd($var){
  echo "<pre>";
  print_r($var);
  echo "</pre>";
}
//starting temporisation
ob_start();

//If a method post is used to submit a formular, go to traitement.php
if (strtoupper($_SERVER['REQUEST_METHOD']) == 'POST') {
    require 'View/traitement.php';
}
//If  the global variable $_GET is set (url)
elseif (isset($_GET['p'])) {
    if ($_GET['p'] == 'inscription') {
    	require 'View/inscription.php';

    } elseif ($_GET['p'] == 'catalogue') {
        //if a product is added to the basket
        if (isset($_GET['id']) && isset($_GET['qte']) && $_GET['qte'] > 0 && isset($_SESSION['panier'])) {
            $_SESSION['panier']->addProduit_id($_GET['id'], $_GET['qte']);
            unset($_GET['id']);
            unset($_GET['qte']);
            header('Location: index.php?p=catalogue');
        }
    	require 'View/catalogue.php';

    } elseif ($_GET['p'] == 'compte') {
    	require 'View/compte.php';

    }elseif ($_GET['p'] == 'deco') {

        session_destroy();
        require 'View/Accueil.php';

    }elseif ($_GET['p'] == 'panier') {
    	require 'View/panier.php';

    }elseif ($_GET['p'] == 'facture') {
    	require 'View/facture.php';

    }elseif ($_GET['p'] == 'login') {
    	require 'View/login.php';
    } elseif ($_GET['p'] == 'admin' && $_SESSION['statut'] == 'admin') {
        require 'View/admin.php';
    }else {
       require 'View/Accueil.php';
   }
}
 else {
    require 'View/Accueil.php';
}
//ending temporisation
$contenu = ob_get_clean();

//loading the template that will include the $contenu content

require 'View/template/template.php';

?>
