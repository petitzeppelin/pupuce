<h1>Panier</h1>
<?php

dd($_SESSION['panier']);


foreach ($_SESSION['panier']->getProduits() as $product_id => $qte) {
    $product = $_SESSION['panier']->getContenu($product_id);
}
 ?>
<div class="row">
    <?php
    $prixTotal = 0;
    foreach ($_SESSION['panier']->getProduits() as $product_id => $qte):
        $product = $_SESSION['panier']->getContenu($product_id);
        $prixTotal += $product->prix * $qte;
         ?>
        <div class="card col-md-3">
            <img class="card-img-top" src="<?= $product->image ?>" alt="Card image cap">
            <div class="card-body">
                <h5 class="card-title"><?= $product->nom ?></h5>
                <p class="card-text"><?= $product->description ?></p>
                <p class="card-text"><?= $product->prix * $qte ?> €</p>
                <p class="card-text">Quantité : <?= $qte ?> unité(s)</p>
                <form method="post" action="index.php">
                    <input type="hidden" name="id" value="<?= $product->id ?>" />
                    <button type="submit" name="SubSupPanier" class="btn btn-danger" value="SubSupPanier">supprimer du panier</button>
                </form>
            </div>
        </div>
    <?php endforeach ;
    if ($_SESSION['panier']->getProduits() === array()) {
        echo "Votre panier est vide";
    } else {
        echo '<form method="post" action="index.php">
                <button type="submit" name="SubValidPanier" class="btn btn-success" value="SubValidPanier">Passer la commande</button>
            </form>';
    }
     echo 'Total : ' .$prixTotal . ' €'; ?>
</div>
