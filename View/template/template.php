<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Boutique</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css"><!-- Le CSS -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css"><!-- FONT AWESOME -->
      <!--  <link rel="stylesheet" type="text/css" href="#"> --> <!-- VOTRE CSS -->
</head>
<body>

    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
      <div class="collapse navbar-collapse justify-content-center" id="navbarSupportedContent">
          <a class="navbar-brand" href="index.php">Chez Pupuce</a>
        <ul class="navbar-nav">
          <li class="nav-item active">
            <a class="nav-link" href="index.php?p=catalogue"> Voir le catalogue </a>
          </li>
          <?php
                if (isset($_SESSION['client'])) {
                    echo "<li class='nav-item active'>
                      <p class='nav-link'>". $_SESSION['client']->nom()." </p>
                    </li>
                    <li class='nav-item active'>
                      <a class='nav-link' href='index.php?p=deco'>Se deconnecter </a>
                    </li>";
                } else {
                    echo "<li class='nav-item active'>
                      <a class='nav-link' href='index.php?p=login'> Se Connecter </a>
                    </li>";
                }
           ?>

          <li class="nav-item active">
            <a class="nav-link" href="index.php?p=inscription"> S'inscrire <span class="sr-only">(current)</span></a>
          </li>
          <li class="nav-item active">
            <a class="nav-link" href="index.php?p=compte"> Gestion du compte <span class="sr-only">(current)</span></a>
          </li>
          <?php
                if (isset($_SESSION['panier'])) {
                    echo "<li class='nav-item active'>
                      <a class='nav-link' href='index.php?p=panier'><i class='fas fa-shopping-cart'></i> : ".$_SESSION['panier']->countProduit()." </a>
                    </li>";
                }
           ?>

        </ul>
      </div>
    </nav>
<?php

if (isset($_SESSION) && isset($_SESSION['panier'])) {
    echo "Panier : ".$_SESSION['panier']->countProduit();
}
 echo $contenu ?>


<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script><!-- JQUERY -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js"></script><!-- POPPER -->
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script><!-- Le JS de BOOTSTRAP -->
<!-- <script type="text/javascript" src="#"></script> --> <!-- VOTRE JS -->
</body>
</html>
