<h1>catalogue</h1>
<?php
if (!isset($_SESSION['panier']) && isset($_GET['id'])) {
    //If the client is not created, this means the client is not logged in
    echo 'Vous devez vous identifier pour mettre des produits dans le panier';
}

echo "<br>";
 ?>
<div class="row">
    <?php
    //list all the products
    // TODO: amélioration : classer par catégorie de produits
    // mettre un systeme de calcul de taxe(TVA)
    foreach (Produits::getInfo() as $product): ?>
        <div class="card col-md-3">
            <img class="card-img-top" src="<?= $product->image ?>" alt="Card image cap">
            <div class="card-body">
                <h5 class="card-title"><?= $product->nom ?></h5>
                <p class="card-text"><?= $product->description ?></p>
                <p class="card-text"><?= $product->prix ?> €</p>
                <form method="get" action="index.php">
                    <label for="qte">Quantité : </label>
                    <input type="number" id="qte" name="qte" placeholder="Min: 1, max: 10"
                        min="1" max="10" />
                    <input type="hidden" name="id" value="<?= $product->id ?>" />
                    <input type="hidden" name="p" value="catalogue" />
                    <button type="submit" class="btn btn-primary">Ajouter au panier</button>
                </form>

            </div>
        </div>
    <?php endforeach ?>
</div>
