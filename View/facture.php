<?php



$facture = $_SESSION['commande']->facture();

$total = 0;
//If the ORder is not empty, print out the content with only name, quantity and price
if ($facture!=array()) {
?>

<h2>Votre Commande</h2>
<div class="container">
<table class="table">
  <thead class="thead-dark">
    <tr>
      <th scope="col">Nom</th>
      <th scope="col">Prix</th>
      <th scope="col">Quantité</th>
      <th scope="col">Prix Total</th>
    </tr>
  </thead>
  <tbody>
      <?php foreach ($facture as $product):
          $total += $product->prix * $product->qte ?>
    <tr>
      <th scope="row"><?= $product->nom ?></th>
      <td><?= $product->prix ?></td>
      <td><?= $product->qte ?></td>
      <td><?= $product->prix * $product->qte ?></td>
    </tr>

    <?php endforeach ?>
  </tbody>
</table>
<h3>Total = <?= $total ?> € </h3>
</div>
<?php
}
unset($_SESSION['commande']);
 ?>
