<h1>inscription</h1>
<div class="container">
<form action="index.php" method="post">
  <div class="form-row">
      <div class="form-group col-md-4">
        <label for="nom">Nom</label>
        <input type="text" class="form-control" id="nom" name='nom' placeholder="nom">
      </div>
      <div class="form-group col-md-4">
        <label for="prenom">Prenom</label>
        <input type="text" class="form-control" id="prenom" name="prenom" placeholder="prenom">
      </div>
      <div class="form-group col-md-4">
        <label for="dateNaissance">Date de naissance</label>
        <input type="date" class="form-control" id="dateNaissance" name="dateNaissance" placeholder="01/01/1990">
      </div>
    <div class="form-group col-md-6">
      <label for="email">Email</label>
      <input type="email" class="form-control" id="email" name="email" placeholder="email">
    </div>
    <div class="form-group col-md-6">
      <label for="mdp">Mot de passe</label>
      <input type="password" class="form-control" id="mdp" name='mdp' placeholder="mot de passe">
    </div>
  </div>
  <div class="form-group">
    <label for="adresse">Addresse</label>
    <input type="text" class="form-control" id="adresse" name="adresse" placeholder="5 rue marcel Bertholy">
  </div>
  <div class="form-row">
    <div class="form-group col-md-6">
      <label for="ville">Ville</label>
      <input type="text" class="form-control" id="ville" name="ville">
    </div>
    <div class="form-group col-md-2">
      <label for="cp">CP</label>
      <input type="text" class="form-control" id="cp" name="cp">
    </div>
  </div>
  <button type="submit" class="btn btn-primary" id="SubInscription" name="SubInscription" value="SubInscription">S'inscrire</button>
</form>
</div>
