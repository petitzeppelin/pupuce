<?php
// Uninstanciated class that describes a personne
abstract class Personne

{
    protected $_nom;
    protected $_prenom;
    protected $_mail;
    protected $_adresse;
    protected $_cp;
    protected $_ville;
    protected $_dateNaissance;
    protected $_id;

    //Constructor takes an array that comes from a formular or from DB
    public function __construct(Array $data){

        $this->hydrate($data);

    }

    //Look for every data in the array
    public function hydrate(array $data)
    {
        //For every key, creates the $method variable with the patern set$key
        foreach ($data as $key => $value) {
            $method = 'set'.ucfirst($key);
            //if the method name exixsts in the current class, execute it with the value argument
            if (method_exists($this, $method)) {
                $this->$method($value);

            }
        }

    }



    //GETTERS
    public function nom(){
        return $this->_nom;
    }
    public function prenom(){
        return $this->_prenom;
    }
    public function mail(){
        return $this->_mail;
    }
    public function adresse(){
        return $this->_adresse;
    }
    public function cp(){
        return $this->_cp;
    }
    public function ville(){
        return $this->_ville;
    }
    public function dateNaissance(){
        return $this->_dateNaissance;
    }
    public function id(){
        return $this->_id;
    }

    //SETTERS
    public function setNom($nom){
        $this->_nom = $nom;
    }
    public function setPrenom($prenom){
        $this->_prenom = $prenom;
    }
    public function setMail($mail){
        $this->_mail = $mail;
    }
    public function setAdresse($adresse){
        $this->_adresse = $adresse;
    }
    public function setCp($cp){
        $this->_cp = $cp;
    }
    public function setVille($ville){
        $this->_ville = $ville;
    }
    public function setDateNaissance($dateNaissance){
        $this->_dateNaissance = $dateNaissance;
    }
    public function setId($id){
        $this->_id = $id;
    }


}
