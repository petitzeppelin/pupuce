<?php

/**
 * Defines a Fournisseur that inherit from Personne and that implements the CRUD interface's method
 */
class Fournisseur extends Personne implements CrudInterface
{
    private $_code_comptable;


    // No constructor , inherit the one from Personne

    //GETTERS
    public function code_comptable(){
        return $this->_code_comptable;
    }

    //SETTERS
    public function setCode_comptable($code_comptable){
        $this->_code_comptable = $code_comptable;
    }

    //CRUD from Crud Interface

    public function New(){
        //Inserting a new fournisseur
        $req = BDD::getBdd()->prepare('INSERT INTO '.get_class($this).
        ' (`nom`,`prenom`, `mail`, `adresse`, `cp`, `ville`, `date_Naissance`, `code_comptable`)
         VALUES
         (\''.$this->nom().'\', \''.$this->prenom().'\', \''.$this->mail().'\', \''.$this->adresse().'\', \''.$this->cp().'\', \''.
         $this->ville().'\', \''.$this->dateNaissance().'\', \''.$this->code_comptable().'\')');
         $req->execute();
         return $req;
    }
    public function Read(){
        //Retrieving data of the current object
        $req = BDD::getBdd()->prepare('Select * FROM ' . get_class($this).
        ' WHERE nom = \'' . $this->_nom . '\' AND mail = \''. $this->_mail . '\' AND code_comptable = \''. $this->_code_comptable. '\'');

        $req->execute();
        $count = $req->rowCount();
        while ($data = $req->fetch(PDO::FETCH_ASSOC)) {
            $this->hydrate($data);
        }

        return $count;

    }
    public function Update(){
        //Updating data of the current object
        $req = BDD::getBdd()->prepare('UPDATE '.get_class($this) . ' SET
        nom = \''.$this->nom().'\',
        prenom = \''.$this->prenom().'\',
        mail = \''.$this->mail().'\',
        adresse = \''.$this->adresse().'\',
        cp = \''.$this->cp().'\',
        ville = \''.$this->ville().'\',
        dateNaissance = \''.$this->dateNaissance().'\',
        code_comptable = \''.$this->code_comptable().'\'
        WHERE id = \'' .$_SESSION['client']->id(). '\'');
         $req->execute();
         return $req;
    }
    public function Delete(){
        //delete the data of the current object
        $req = BDD::getBdd()->prepare( 'DELETE FROM ' . get_class($this) . ' WHERE id = \'' . $this->_id . '\'' );
        $req->execute();
        return $req;
    }
}
