<?php

/**
 * Class Autoloader
 */
class Autoloader{

    /**
     * Registering this autoloader
     */
    static function register(){
        spl_autoload_register(array(__CLASS__, 'autoload'));
    }

    /**
     * Requiring the class file
     * @param $class string classname that has to be loaded
     */
    static function autoload($class){
        require 'app/' . $class . '.php';
    }

}
