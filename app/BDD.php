<?php
abstract class BDD{
    //Uninstanciated class , the connexion is called directly from the class (static)
    private static $_bdd;
    //amelioration possible : stocker ces attributs dans fichier config à part
    private static $_host = 'localhost';
    private static $_dbname = 'boutique';
    private static $_username = 'root';
    private static $_pass = '';

    //setter of the property bdd that stores the PDO instance
    private static function setBdd(){
        try {
            self::$_bdd = new PDO('mysql:host='.self::$_host.';dbname='.self::$_dbname.';charset=utf8',self::$_username, self::$_pass);
            self::$_bdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING);
        } catch (PDOException $e) {
            die('<h1>Impossible de se connecter a la base de donnee</h1>');
        }
    }

    // retrieve the property bdd (PDO) or if it's set to null, creates it
    static function getBdd(){
        if (self::$_bdd == null) {
            self::setBdd();

        }
        return self::$_bdd;
    }
}
