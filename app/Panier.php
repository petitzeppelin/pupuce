<?php

/**
 * Defines a basket that will be instanciated when a client login is successfull
 */
class Panier
{
    private $_client_id;
    private $_produit_id = array();
    private $_num_panier;

    public function __construct()
    {
        $this->_client_id = $_SESSION['client']->id();
        $this->getNumPanier();

    }

    //GETTERS
    public function num_panier(){
        return $this->_num_panier;
    }
    public function client_id(){
        return $this->_client_id;
    }
    public function getProduits(){
        return $this->_produit_id;
    }
    //SETTERS
    public function getNumPanier(){
        //Retrieve a basket number from the database
        // TODO: CHanger vers autoincrementation
        $req = BDD::getBdd()->prepare('SELECT MAX(num_panier) AS max_num FROM Panier');
        $req->execute();
        $data = $req->fetch(PDO::FETCH_OBJ);


        $data->max_num+=1;

        $this->_num_panier = $data->max_num;
    }

    // Checking if the id exists in the database
    public function addProduit_id($id, $qte){
        $req = BDD::getBdd()->prepare('SELECT id FROM Produits WHERE id = '.$id);
        $req->execute();
        $count = $req->rowCount();
        if ($count == 1) {
            $this->_produit_id[$id] += $qte;
        }
    }

    //Return total quantity of products in this basket
    public function countProduit(){
        $qte = 0;
        if ($this->_produit_id != array()) {
            foreach ($this->_produit_id as $key => $value) {
                $qte +=$value;
            }
        }
        return $qte;
    }

    //Retrieving informations about a product with its Id
    public function getContenu($id){

        $req = BDD::getBdd()->prepare('SELECT * FROM Produits WHERE id = '.$id);
        $req->execute();
        $data = $req->fetch(PDO::FETCH_OBJ);

        return $data;

    }

    //Deleting a product stored in this basket
    public function deleteProduit($id){
        if (array_key_exists($id,$this->_produit_id)) {
            unset($this->_produit_id[$id]);
            return true;
        } else {
            return false;
        }
    }

    //Inserting the products ordered in DB (Panier & Panier_Produit)
    public function store(){

         $req = BDD::getBdd()->prepare('INSERT INTO '.get_class($this).
         ' (`num_panier`,`client_id`)
          VALUES (:num_panier, :client_id)');
         $req->bindParam(":num_panier",$this->_num_panier) ;
         $req->bindParam(":client_id",$this->_client_id) ;
         $req->execute();

         foreach ($this->_produit_id as $id => $qte) {
             $sql = "INSERT INTO Panier_Produit (`num_panier`,`id_produit`, `qte`)
              VALUES (:num_panier, :id_produit, :qte)";
             $req1 = BDD::getBdd()->prepare($sql);
             $req1->bindParam(":num_panier",$this->_num_panier);
             $req1->bindParam(":id_produit",$id);
             $req1->bindParam(":qte",$qte);
             $req1->execute();
         }
    }


}
