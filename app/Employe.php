<?php

/**
 * Defines an employee that inherit from Personne and that implements the CRUD interface's method
 */
class Employe extends Personne implements CrudInterface
{
    private $_num_secu;
    private $_fonction;
    private $_salaire;
    private $_superieur;

    // No constructor , inherit the one from Personne

    //GETTERS
    public function num_secu(){
        return $this->_num_secu;
    }
    public function fonction(){
        return $this->_fonction;
    }
    public function salaire(){
        return $this->_salaire;
    }
    public function superieur(){
        return $this->_superieur;
    }

    //SETTERS
    public function setNum_secu($num_secu){
        $this->_num_secu = $num_secu;
    }
    public function setFonction($fonction){
        $this->_fonction = $fonction;
    }
    public function setSalaire($salaire){
        $this->_salaire = $salaire;
    }
    public function setSuperieur($superieur){
        $this->_superieur = $superieur;
    }



    //CRUD from Crud Interface

    public function New(){
        //Inserting a new employee
        $req = BDD::getBdd()->prepare('INSERT INTO '.get_class($this).
        ' (`nom`,`prenom`, `mail`, `adresse`, `cp`, `ville`, `date_Naissance`, `num_secu`, `superieur`)
         VALUES
         (\''.$this->nom().'\', \''.$this->prenom().'\', \''.$this->mail().'\', \''.$this->adresse().'\', \''.$this->cp().'\', \''.
         $this->ville().'\', \''.$this->dateNaissance().'\', \''.$this->num_secu().'\', \''.$this->superieur(). '\')');
         $req->execute();
         return $req;
    }
    public function Read(){
        //Retrieving data of the current object
        $req = BDD::getBdd()->prepare('Select * FROM ' . get_class($this).
        ' WHERE nom = \'' . $this->_nom . '\' AND mail = \''. $this->_mail . '\' AND num_secu = \''. $this->_num_secu. '\'');
        $req->execute();
        $count = $req->rowCount();
        while ($data = $req->fetch(PDO::FETCH_ASSOC)) {
            $this->hydrate($data);
        }
        return $count;

    }
    public function Update(){
        //Updating data of the current object
        $req = BDD::getBdd()->prepare('UPDATE '.get_class($this) . ' SET
        nom = \''.$this->nom().'\',
        prenom = \''.$this->prenom().'\',
        mail = \''.$this->mail().'\',
        adresse = \''.$this->adresse().'\',
        cp = \''.$this->cp().'\',
        ville = \''.$this->ville().'\',
        dateNaissance = \''.$this->dateNaissance().'\',
        fonction = \''.$this->fonction().'\',
        salaire = \''.$this->salaire().'\',
        superieur = \''.$this->superieur().'\',
        num_secu = \''.$this->num_secu().'\'
        WHERE id = \'' .$_SESSION['client']->id(). '\'');
         $req->execute();
         return $req;
    }
    public function Delete(){
        //delete the data of the current object
        $req = BDD::getBdd()->prepare( 'DELETE FROM ' . get_class($this) . ' WHERE id = \'' . $this->_id . '\'' );
        $req->execute();
        return $req;
    }
}
