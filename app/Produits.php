<?php

/**
 * Defines a product
 */
class Produits
{
    private $_id;
    private $_nom;
    private $_description;
    private $_image;
    private $_prix;
    private $_qte_stock;

    //Constructor takes an array that comes from a formular or from DB
    public function __construct(Array $data){

        $this->hydrate($data);

    }
    //Look for every data in the array
    public function hydrate(array $data)
    {
        //For every key, creates the $method variable with the patern set$key
        foreach ($data as $key => $value) {
            $method = 'set'.ucfirst($key);
            //if the method name exixsts in the current class, execute it with the value argument
            if (method_exists($this, $method)) {
                $this->$method($value);

            }
        }

    }

    //GETTERS
    public function nom(){
        return $this->_nom;
    }
    public function id(){
        return $this->_id;
    }
    public function description(){
        return $this->_description;
    }
    public function image(){
        return $this->_image;
    }
    public function prix(){
        return $this->_prix;
    }
    public function qte_stock(){
        return $this->_qte_stock;
    }

    //SETTERS
    public function setId($id){
        $this->_id = $id;
    }
    public function setNom($nom){
        $this->_nom = $nom;
    }
    public function setDescription($description){
        $this->_description = $description;
    }
    public function setImage($image){
        $this->_image = $image;
    }
    public function setPrix($prix){
        $this->_prix = $prix;
    }
    public function setQte_stock($qte_stock){
        $this->_qte_stock = $qte_stock;
    }

    //Inserting a new product in DB
    public function createProduit(){
        $req = BDD::getBdd()->prepare('INSERT INTO '.get_class($this).
        ' (`nom`,`description`, `image`, `prix`, `qte_stock`)
         VALUES
         (\''.$this->nom().'\', \''.$this->description().'\', \''.$this->image().'\', \''.$this->prix().'\', \''.$this->qte_stock(). '\')');
         $req->execute();
         return $req;
    }

    //Static Retrieving all the informations from the table that has the current classname
    public static function getInfo(){
        $req = BDD::getBdd()->prepare('Select * FROM ' . get_called_class());

        $req->execute();

        $data = $req->fetchAll(PDO::FETCH_OBJ);

        return $data;
    }

    ////Updating data of the current object
    public function Update(){
        $req = BDD::getBdd()->prepare('UPDATE '.get_class($this) . ' SET
        nom = \''.$this->nom().'\',
        description = \''.$this->description().'\',
        image = \''.$this->image().'\',
        prix = \''.$this->prix().'\',
        qte_stock = \''.$this->qte_stock().'\'

        WHERE id = \'' .$this->id(). '\'');
         $req->execute();
         return $req;
    }
    //delete the data of the current object
    public function Delete(){

        $req = BDD::getBdd()->prepare( 'DELETE FROM ' . get_class($this) . ' WHERE id = \'' . $this->_id . '\'' );
        $req->execute();
        return $req;
    }
}
