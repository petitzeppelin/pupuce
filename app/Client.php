<?php

/**
 * Defining a client that inherit from Personne and that implements the CRUD interface's method
 */
class Client extends Personne implements CrudInterface
{
    private $_date_crea_compte;
    private $_mdp;

    // No constructor , inherit the one from Personne

    //GETTERS
    public function date_crea_compte(){
        return $this->_date_crea_compte;
    }
    public function mdp(){
        return $this->_mdp;
    }

    //SETTERS
    public function setDate_crea_compte($date_crea_compte){
        $this->_date_crea_compte = $date_crea_compte;
    }
    public function setMdp($mdp){
        $this->_mdp = $mdp;
    }

    //CRUD from Crud Interface
    public function New(){
        //Inserting a new client
        $req = BDD::getBdd()->prepare('INSERT INTO '.get_class($this).
        ' (`nom`,`prenom`, `mail`, `adresse`, `cp`, `ville`, `dateNaissance`, `date_crea_compte`, `mdp`)
         VALUES
         (\''.$this->nom().'\', \''.$this->prenom().'\', \''.$this->mail().'\', \''.$this->adresse().'\', \''.$this->cp().'\', \''.
         $this->ville().'\', \''.$this->dateNaissance().'\', \''.$this->date_crea_compte().'\', \''.$this->mdp(). '\')');
         $req->execute();
         return $req;
    }

    public function Read(){
        //Retrieving data of the current object
        $req = BDD::getBdd()->prepare('Select * FROM ' . get_class($this).
        ' WHERE nom = \'' . $this->_nom . '\' AND mail = \''. $this->_mail . '\' AND mdp = \''. $this->_mdp. '\'');
        $req->execute();
        $count = $req->rowCount();
        while ($data = $req->fetch(PDO::FETCH_ASSOC)) {
            $this->hydrate($data);
        }
        return $count;

    }
    public function Update(){
        //Updating data of the current object
        $req = BDD::getBdd()->prepare('UPDATE '.get_class($this) . ' SET
        nom = \''.$this->nom().'\',
        prenom = \''.$this->prenom().'\',
        mail = \''.$this->mail().'\',
        adresse = \''.$this->adresse().'\',
        cp = \''.$this->cp().'\',
        ville = \''.$this->ville().'\',
        dateNaissance = \''.$this->dateNaissance().'\',
        mdp = \''.$this->mdp().'\'
        WHERE id = \'' .$_SESSION['client']->id(). '\'');
         $req->execute();
         return $req;
    }
    public function Delete(){
        //delete the data of the current object
        $req = BDD::getBdd()->prepare( 'DELETE FROM ' . get_class($this) . ' WHERE id = \'' . $this->_id . '\'' );
        $req->execute();
        return $req;
    }
}
