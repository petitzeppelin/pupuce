<?php

/**
 *Class representing the Order done by a logged in client
 */
class Commande
{

    private $_date_commande;
    private $_date_livraison;
    private $_panier;

    function __construct(Panier $panier)
    {
        //An order  is relative to a basket so it's passed to the constructor
        $this->_panier = $panier;
        $this->_date_commande = date('Y-m-d');
        $this->_date_livraison = date('Y-m-d',mktime(0, 0, 0, date("m")  , date("d")+15, date("Y")));
    }

    //GETTERS
    public function date_commande(){
        return $this->_date_commande;
    }

    public function num_commande(){
        return $this->_num_commande;
    }

    public function date_livraison(){
        return $this->_date_livraison;
    }
    public function panier(){
        return $this->_panier;
    }


    //Inserting the Order
    public function Create(){
        $req = BDD::getBdd()->prepare('INSERT INTO '.get_class($this).
        ' (`date_commande`, `date_livraison`, `num_panier`)
         VALUES
         (\''.$this->date_commande().'\', \''.$this->date_livraison().'\', \''.$this->_panier->num_panier(). '\')');
         $req->execute();
    }

    //Retrieving data name, price and quantity of products for the current Order
    public function facture(){
        $sql = 'SELECT `nom`, `prix`, `qte` FROM Produits
        LEFT JOIN Panier_Produit ON Produits.id = Panier_Produit.id_produit
        LEFT JOIN Panier ON Panier_Produit.num_panier = Panier.num_panier
        WHERE Panier.num_panier = '.$_SESSION['commande']->panier()->num_panier().' AND Panier.client_id = '.$_SESSION['commande']->panier()->client_id();
        $req = BDD::getBdd()->prepare($sql);
        $req->execute();
        $data = $req->fetchAll(PDO::FETCH_OBJ);
        return $data;
    }
}
