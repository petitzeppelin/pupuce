<?php

/**
 *Defines the methods that have to be implemented by all the Personne's Children
 */
interface CrudInterface
{
    public function New();
    public function Read();
    public function Update();
    public function Delete();
}
